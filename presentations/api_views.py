import json
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .models import Presentation
from events.models import Conference
from common.json import ModelEncoder
from django.shortcuts import get_object_or_404

class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties  = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "status",
    ]
    def get_extra_data(self, o):
        return { "status": o.status.name }

class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "status",
    ]

    def get_extra_data(self, o):
        return { "status": o.status.name }   

@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
            {"presentations": presentations},
            encoder=PresentationListEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        presentation = Presentation.objects.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_presentation(request, id):
    presentation = get_object_or_404(Presentation, id=id)
    if request.method == "GET":
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        for key, value in content.items():
            setattr(presentation, key, value)
        presentation.save()
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        presentation.delete()
        return JsonResponse({"deleted": True}, status=204)
