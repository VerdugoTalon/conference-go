from django.urls import path
from .api_views import api_list_attendees, api_show_attendee

urlpatterns = [
    path('conferences/<int:conference_id>/attendees/', api_list_attendees, name='attendee-list'),
    path('conferences/<int:conference_id>/attendees/<int:id>/', api_show_attendee, name='attendee-detail'),
]
